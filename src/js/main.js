console.log('Its work');

// Swiper Options
var mainSwiper = new Swiper ('.swiper-container', {
    pagination: {
      el: '.swiper-pagination',
    }    
})

// Video play
document.querySelector(".button-play").addEventListener("click", (event) => {
  const video = document.querySelector(".video video");
  video.style.display = "block";  
  video.play();
  document.querySelector(".button-play").style.display = "none";
});

